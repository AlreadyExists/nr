#include <stdafx.h>
#include <iostream>
#include <vector>

using namespace std;
class Robot
    {
    public: 
        virtual Robot* clone() = 0;
        virtual void Get_Robot_Info() = 0; 
        virtual ~Robot() {};
    };

class ArmedRobot: public Robot
    {

    public: 
        Robot* clone() { 
            return new ArmedRobot( *this); 
            }
        void Get_Robot_Info() { 
            cout << "ArmedRobot here" << endl; 
            } 

        ArmedRobot() {} 
    };

class FlyingRobot: public Robot
    {

    public: 
        Robot* clone() { 
            return new FlyingRobot( *this); 
            } 
        void Get_Robot_Info() { 
            cout << "FlyingRobot here" << endl; 
            }

        FlyingRobot() {} 
    };

class CraftyRobot: public Robot
    {

    public: 
        Robot* clone() { 
            return new CraftyRobot( *this); 
            }
        void Get_Robot_Info() { 
            cout << "CraftyRobot here" << endl; 
            }

        CraftyRobot() {}
    };

class RoboBox
    {
    private:
        ArmedRobot* ArmedRobotPrototype;
        FlyingRobot* FlyingRobootPrototype;
        CraftyRobot* CraftyRobotPrototype;
    public: 
        RoboBox(){
            ArmedRobotPrototype=new ArmedRobot();
            FlyingRobootPrototype=new FlyingRobot();
            CraftyRobotPrototype=new CraftyRobot();
            }

        Robot* Get_ArmedRobot() { 
            return ArmedRobotPrototype->clone();
            }
        Robot* Get_FlyingRobot() { 
            return FlyingRobootPrototype->clone();
            }
        Robot* Get_CraftyRobot() { 
            return CraftyRobotPrototype->clone();
            }
    };

int main()
{ 
	RoboBox magicplace;
	vector<Robot*> storage;
	storage.push_back( magicplace.Get_ArmedRobot());
	storage.push_back( magicplace.Get_ArmedRobot());
	storage.push_back( magicplace.Get_FlyingRobot());
	storage.push_back( magicplace.Get_CraftyRobot());


	for(int i=0; i<storage.size(); i++)
		storage[i]->Get_Robot_Info();
	return 0;
}